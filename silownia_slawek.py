#Program na siłownie
from random import randrange

sześćdziesiątR =     ['3x12', '5x12', '3x24', '4x24', '4x36', '5x36', '4x48', '4x60', '5x60']
trzydzieściR =       ['3x6', '5x6', '3x12', '4x12', '4x18', '5x18', '4x24', '4x30',  '5x30']
piętnaścieR =        ['3x3', '5x3', '3x6', '4x6', '4x9', '5x9', '4x12', '4x15', '5x15']
dziesięćR =          ['3x2', '5x2', '3x4', '4x4', '4x6', '5x6', '4x8', '4x10', '5x10']
pięćR =              ['3x1', '5x1', '3x2', '4x2', '4x3', '5x3', '4x4', '4x5', '5x5']
MobipięćR =          [ 3, 3, 3, 4, 4, 4, 5, 5, 5]
MobitrzydzieściSek = [10, 10, 10, 20, 20, 20, 30, 30, 30]


excercises = {
    1:[#tier 1
        #day 1
        [('push up', piętnaścieR), ('stick behind', pięćR), ('Incline Row ring on hips lenght', piętnaścieR),
         ('One arm upright Lat Lean', trzydzieściR), ('Bent Hollow Body Rock', sześćdziesiątR), ('Back Table', trzydzieściR)],
        #day 2
        [('Forearm Plank', piętnaścieR), ('Running Man', dziesięćR), ('Elbow Side Plank Twist', dziesięćR), ('Standing Hip Circle', pięćR), ('Straddle Up', piętnaścieR),
         ('chair hamstring press', pięćR)]
        
    ],
    2:[ #tier 2
        #day 1
        [('push up', piętnaścieR), ('stick behind', pięćR), ('Incline Row ring on hips lenght', piętnaścieR),
         ('One arm upright Lat Lean', trzydzieściR), ('Bent Hollow Body Rock', sześćdziesiątR), ('Back Table', trzydzieściR)],
        #day 2
        [('Forearm Plank', piętnaścieR), ('Running Man', dziesięćR), ('Elbow Side Plank Twist', dziesięćR), ('Standing Hip Circle', pięćR), ('Straddle Up', piętnaścieR),
         ('chair hamstring press', pięćR)]
    ],
    3:[#tier 3
        #day 1
        [('push up', piętnaścieR), ('stick behind', pięćR), ('Incline Row ring on hips lenght', piętnaścieR),
         ('One arm upright Lat Lean', trzydzieściR), ('Bent Hollow Body Rock', sześćdziesiątR), ('Back Table', trzydzieściR)],
        #day 2
        [('Forearm Plank', piętnaścieR), ('Running Man', dziesięćR), ('Elbow Side Plank Twist', dziesięćR), ('Standing Hip Circle', pięćR), ('Straddle Up', piętnaścieR),
         ('chair hamstring press', pięćR)]
        
    ],
    4:[  # tier 4
        #day 1
        [('Negative Dips 10sec/bench Dips', dziesięćR), ('Arm Circles', pięćR), ('Hinge Row', dziesięćR), ('Elbow Lean + Stick', pięćR), ('Stradle hollow body rock', sześćdziesiątR), ('Back Table Trun', dziesięćR)],
        #day 2
        [('One Arm Plank', trzydzieściR), ('Running Man', dziesięćR), ('Arch Up', dziesięćR), ('Standing trunk circle', pięćR), ('tuck hanging leg lift', piętnaścieR), ('jefferosn Curls', pięćR)]
        
    ]
    #TODO: reszta tierow
}

class Finished(BaseException):pass

def try_again():
    
    again = input("Spróbujesz ponownie (t/n)? ")
    if (str.upper(again) == 'T'):
        return True
    else: 
        print("źle wprowadzona odpowiedz, następnym razem spróbuj litere t dla ponownej próby lub n aby przerwać program")
        raise Finished
 
    #TODO: jesli 't' zwraca true, w przeciwnym razie wyjatek Finished
    #TODO: przy nie prawidlowym wpisie wyswietl informacje o bledzie przed zakonczeniem
   

def say_goodbye():
    goodbyes = ['Do zobacznia!', 'Do następnego!', 'Do widzenia!']
    print (goodbyes[randrange(0,2)])
    
    #TODO: wypisz losowe pozegnanie

def input_int(prompt, valid_range):
    #TODO: wyswietl prompt, wraz z informacja o poprawnym zakresie
    #TODO: wczytaj liczbe, sprawdz czy jest poprawna
    #TODO: jesli sie powiedzie to zwroc wczytana liczbe, jesli nie to:
        
    again = True
    while again == True:  
        try:
            print(prompt, valid_range)
            bla = int(input())
            if bla in valid_range:
                again = False
                return bla
            else:
                try_again()
        except ValueError:
            print('Invalid input') 
            try_again()

def do_excercise(tier, day, step):
    #TODO: wypisz cwiczenia dla danych parametrow
    for x in excercises[tier][day-1]:
        print(x[0], ":", x[1][step-1])
         
    #print(excercises[tier][day][step])
    
    #print(excercises[2][1][2])
def main():
    try:
        step = input_int('Wpisz wykonywany step:', range(1, 10))
        dzien = input_int('Wpisz wykonywany dzień: ', range(1, 3)) 
        tier = input_int('Wpisz wykonywany tier: ', range(1,5))
        print(step, dzien, tier)
        do_excercise(tier, dzien, step)
    except Finished: say_goodbye()


if __name__ == '__main__':
    main()
                  
