#9) Znajduje n-tą liczbę w ciągu liczb Fibonacciego
'''def fib(n):
    liczby = [0,1]
    if n<=0:
            return 'wrong element'
    for x in range(0,n-1):
        y = liczby[-1]+liczby[-2]
        liczby.append(y)
    return liczby[-1]
'''
def fib(n):
    ostatnia=1
    przedostatnia=0
    if n<=0:
        return 'wrong element'
    for x in range(0, n-1):
        y= ostatnia + przedostatnia
        przedostatnia=ostatnia
        ostatnia = y
    return ostatnia       

A=1
l_fib=fib(A)
print(l_fib)
A=4
l_fib=fib(A)
print(l_fib)
A=8
l_fib=fib(A)
print(l_fib)
A=15
l_fib=fib(A)
print(l_fib)
A=0
l_fib=fib(A)
print(l_fib)
A=-2
l_fib=fib(A)
print(l_fib)