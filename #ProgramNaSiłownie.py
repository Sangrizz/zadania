import sys
import getpass


def Password(Hasło):
    if p.lower() == Hasło:
        print('Welcome..!!!')
    else:
        print('The answer entered by you is incorrect..!!!')
        print(p)
        sys.exit()


def AgainAndAgain(Again):
    if (str.upper(Again) == 'N') or (str.upper(Again) == 'T'):
        pass
    else:
        print('wpisanie czegoś innego niż (t/n) automatycznie kończy działanie programu')
        sys.exit()


#Program na siłownie
sześćdziesiątSek = {1: '3x12', 2: '5x12', 3: '3x24', 4: '4x24',
                    5: '4x36', 6: '5x36', 7: '4x48', 8: '4x60', 9: '5x60'}
sześćdziesiątR = {1: '3x12', 2: '5x12', 3: '3x24', 4: '4x24',
                  5: '4x36', 6: '5x36', 7: '4x48', 8: '4x60', 9: '5x60'}
trzydzieściR = {1: '3x6', 2: '5x6', 3: '3x12', 4: '4x12',
                5: '4x18', 6: '5x18', 7: '4x24', 8: '4x30', 9: '5x30'}
piętnaścieR = {1: '3x3', 2: '5x3', 3: '3x6', 4: '4x6',
               5: '4x9', 6: '5x9', 7: '4x12', 8: '4x15', 9: '5x15'}
dziesięćR = {1: '3x2', 2: '5x2', 3: '3x4', 4: '4x4',
             5: '4x6', 6: '5x6', 7: '4x8', 8: '4x10', 9: '5x10'}
pięćR = {1: '3x1', 2: '5x1', 3: '3x2', 4: '4x2',
         5: '4x3', 6: '5x3', 7: '4x4', 8: '4x5', 9: '5x5'}
MobipięćR = {1: 3, 2: 3, 3: 3, 4: 4, 5: 4, 6: 4, 7: 5, 8: 5, 9: 5}
MobitrzydzieściSek = {1: 10, 2: 10, 3: 10,
                      4: 20, 5: 20, 6: 20, 7: 30, 8: 30, 9: 30}
powtórzeniaKobiet = {1: '6-8pow'}


def ćwiczenia5(tabelka, dzień):
    tier5dzien1 = {'Negative Dips 10sec/dipsy na kołach': pięćR[tabelka], 'Candlestick': pięćR[tabelka], 'Bent Arm Chin Hang': trzydzieściR[tabelka],
                   'Elbow Lean + Stick': pięćR[tabelka], 'HollowBody': sześćdziesiątSek[tabelka], 'ReversBackArch': MobitrzydzieściSek[tabelka]}
    tier5dzien2 = {'Elevated_planche_Lean': trzydzieściR[tabelka], 'Running_Man': dziesięćR[tabelka], 'Arch Up + Rotation': dziesięćR[tabelka],
                   'Standing Lateral Reach': pięćR[tabelka], 'hanging leg lift 90-45': pięćR[tabelka], 'Bar lean streching - horizontal': pięćR[tabelka]}
 
    if dzień == 1:
        return tier5dzien1
    else:
        return tier5dzien2


def ćwiczenia4(tabelka, dzień):
    tier4dzien1 = {'Negative Dips 10sec/bench Dips': dziesięćR[tabelka], 'Arm Circles': pięćR[tabelka], 'Hinge Row': dziesięćR[tabelka],
                   'Elbow Lean + Stick': pięćR[tabelka], 'Stradle hollow body rock': sześćdziesiątSek[tabelka], 'Back Table Trun': dziesięćR[tabelka]}
    tier4dzien2 = {'One Arm Plank': trzydzieściR[tabelka], 'Running Man': dziesięćR[tabelka], 'Arch Up': dziesięćR[tabelka],
                   'Standing trunk circle': pięćR[tabelka], 'tuck hanging leg lift': piętnaścieR[tabelka], 'jefferosn Curls': pięćR[tabelka]}
    if dzień == 1:
        return tier4dzien1
    else:
        return tier4dzien2


def ćwiczenia2(tabelka, dzień):
    tier2dzien1 = {'push up': piętnaścieR[tabelka], 'stick behind': pięćR[tabelka], 'Incline Row ring on hips lenght': piętnaścieR[tabelka],
                   'One arm upright Lat Lean': trzydzieściR[tabelka], 'Bent Hollow Body Rock': sześćdziesiątR[tabelka], 'Back Table': trzydzieściR[tabelka]}
    tier2dzien2 = {'Forearm Plank': piętnaścieR[tabelka], 'Running Man': dziesięćR[tabelka],
                   'Elbow Side Plank Twist': dziesięćR[tabelka], 'Standing Hip Circle': pięćR[tabelka], 'chair hamstring press': pięćR[tabelka]}
    if dzień == 1:
        return tier2dzien1
    else:
        return tier2dzien2


def kobietyćw1(tabelka, tier):
    tier1 = {'Przysiad z Dłońmi na ziemi': powtórzeniaKobiet[tabelka], 'Łabądź': powtórzeniaKobiet[tabelka], 'Wspięcia nóg na podwyższeniu / jedno lub obu nóż': powtórzeniaKobiet[tabelka], 'Pompka na podwyższeniu / z kolan': powtórzeniaKobiet[tabelka],
             'Dipsy na krześle': powtórzeniaKobiet[tabelka], 'Świecznik gimnastryczny': powtórzeniaKobiet[tabelka], 'Wiosłowanie/ odepchnięcia na łokciach/ wznosy na łopatkach': powtórzeniaKobiet[tabelka], 'Gimnastyczne brzuszki': powtórzeniaKobiet[tabelka]}
    tier2 = {'Wykroki': powtórzeniaKobiet[tabelka], 'Boczny plank na przywodziciele': powtórzeniaKobiet[tabelka], 'Boczny plank na odwodziciele': powtórzeniaKobiet[tabelka], 'Wspięcia pleców':
             powtórzeniaKobiet[tabelka], 'Wspinaczka- W OPÓR': powtórzeniaKobiet[tabelka], 'Rotacje w wykroku w podporze jednorącz': powtórzeniaKobiet[tabelka], 'Martwy Robak': powtórzeniaKobiet[tabelka]}
    if tier == 1:
        return tier1
    elif tier == 2:
        return tier2
    else:
        return 'Podany tier nie istnieje'


TryAgain = True
while TryAgain:
    P = input("Wpisz płeć K/M: ")
    if (str.upper(P) == 'K') or (str.upper(P) == 'M'):
        TryAgain = False
    else:
        print("Aby przejść do następnej opcji wpisz 'K' lub 'M'")
if (str.upper(P) == 'M'):
    TryAgain = True
    while TryAgain:
        try:
            A = int(input("Wpisz wykonywany step(cyframi): "))
            try:
                if A > 0 and A < 10:
                    pass
                else:
                    print('źle podany dzień. Proszę wpisać cyfrę 1 lub 9')
                    continue
            except ValueError:
                print('Musisz podać liczbę z przedziału od 1 do 9')
                try:
                    Again = input("Spróbujesz ponownie (t/n)? ")
                    AgainAndAgain(Again)
                except:
                    print("Do zobacznia!")
                    TryAgain = False
                    sys.exit()
                else:
                    if (str.upper(Again) == 'N'):
                        print('Do następnego!')
                        TryAgain = False
                        sys.exit()
        except ValueError:
            print('Musisz podać liczbę z przedziału od 1 do 9')
            try:
                Again = input("Spróbujesz ponownie (t/n)? ")
                AgainAndAgain(Again)
            except:
                print("Do zobacznia!")
                TryAgain = False
                sys.exit()
            else:
                if (str.upper(Again) == 'N'):
                    print('Do następnego!')
                    TryAgain = False
                    sys.exit()
        else:
            TryAgain = False
    TryAgain = True
    while TryAgain:
        try:
            B = int(input("Wpisz wykonywany dzień(cyframi): "))
            try:
                if B == 1 or B == 2:
                    pass
                else:
                    print('źle podany dzień. Proszę wpisać cyfrę 1 lub 2')
                    continue
            except ValueError:
                print('Musisz podać liczbę z przedziału od 1 do 2')
                try:
                    Again = input("Spróbujesz ponownie (t/n)? ")
                    AgainAndAgain(Again)
                except:
                    print("Do zobacznia!")
                    TryAgain = False
                    sys.exit()
                else:
                    if (str.upper(Again) == 'N'):
                        print('Do następnego!')
                        TryAgain = False
                        sys.exit()
        except ValueError:
            print('Musisz podać liczbę z przedziału od 1 do 5')
            try:
                Again = input("Spróbujesz ponownie (t/n)? ")
                AgainAndAgain(Again)
            except:
                print("Do zobacznia!")
                TryAgain = False
                sys.exit()
            else:
                if (str.upper(Again) == 'N'):
                    print('Do następnego!')
                    TryAgain = False
                    sys.exit()
        else:
            TryAgain = False
    TryAgain = True
    while TryAgain:
        try:
            T = int(input("Wpisz wykonywany tier(cyframi): "))
            if T == 5:
                p = getpass.getpass(prompt='Miasto ulubionych Świętych? ')
                a = 'boston'
                Password(a)
                baz = ćwiczenia5(A, B)
                TryAgain = False
            elif T == 4:
                p = getpass.getpass(
                    prompt="Uzupełnij: 'Czy ktoś dał znać o imprezie......?' ")
                a = 'ramzesowi'
                Password(a)
                baz = ćwiczenia4(A, B)
                TryAgain = False
            elif T == 2:
                p = getpass.getpass(
                    prompt='Tytuł głowy La Familii Biernatti? ')
                a = 'don'
                Password(a)
                baz = ćwiczenia2(A, B)
                TryAgain = False
            else:
                print("Proszę wprowadzić poprawny tier(1,2,3,4,5)")
                Again = input("Spróbujesz ponownie (t/n)? ")
                AgainAndAgain(Again)
                if (str.upper(Again) == 'N'):
                    print('Do widzenia!')
                    sys.exit()
                else:
                    continue
        except ValueError:
            print('Musisz podać liczbę z przedziału od 1 do 5')
            try:
                Again = input("Spróbujesz ponownie (t/n)? ")
                AgainAndAgain(Again)
            except:
                print("Do zobacznia!")
                TryAgain = False
                sys.exit()
            else:
                if (str.upper(Again) == 'N'):
                    print('Do następnego!')
                    TryAgain = False
                    sys.exit()
        else:
            TryAgain = False
elif (str.upper(P) == 'K'):
    TryAgain = True
    while TryAgain:
        try:
            T = int(input("Wpisz wykonywany tier(cyframi): "))
            if T == 1:
                p = getpass.getpass(
                    prompt="Kto jest bohaterem Vegasowej wersji piosenki 'Barbra Streisand' ")
                a = 'arek'
                Password(a)
            elif T == 2:
                p = getpass.getpass(
                    prompt="Nakrycie głowy które uzupełniłoby wygląd Gega na weselu?... ")
                a = 'kaszkiet'
                Password(a)
            else:
                print('Istnieje tylko tier 1 oraz 2')
                continue

        except ValueError:
            print('Musisz podać liczbę z przedziału od 1 do 2')
            try:
                Again = input("Spróbujesz ponownie (t/n)? ")
                AgainAndAgain(Again)
            except:
                print("Do zobacznia!")
                TryAgain = False
                sys.exit()
            else:
                if (str.upper(Again) == 'N'):
                    print('Do następnego!')
                    TryAgain = False
                    sys.exit()
        else:
            TryAgain = False
    TryAgain = True
    while TryAgain:
        try:
            A = int(input("Dla 6-8 powtórzeń wpisz 1: "))
            if A == 1:
                pass
            else:
                print('Istnieją tylko powtórzenia dla 1')
                continue
        except ValueError:
            print(
                'Wystąpił błąd przy wpisaniu przez użytkownika. Powtórz operacje i wprowadz sugerowane cyfry')
            try:
                Again = input("Spróbujesz ponownie (t/n)? ")
                AgainAndAgain(Again)
            except:
                print("Do zobacznia!")
                TryAgain = False
                sys.exit()
            else:
                if (str.upper(Again) == 'N'):
                    print('Do następnego!')
                    TryAgain = False
                    sys.exit()
        else:
            TryAgain = False

    baz = kobietyćw1(A, T)


print(baz)
