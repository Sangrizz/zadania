#3) Wylicza odległość euclidesową pomiędzy dwoma punktami. Argumenty do cztery floaty - współrzędne x,y obu punktów
def get_distance(x1, y1, x2, y2):
    if x1<x2:
        x=x2-x1
        if y1<y2:
            y=y2-y1
        else: y=y1-y2
    else:
        x=x1-x2
        if y1>y2:
            y=y1-y2
        else: y=y2-y1 
    od_euc_kw=(x**2 + y**2)**(1/2)
    return od_euc_kw
    
x_p1=10
y_p1=-1
x_p2=0
y_p2=-4
odległość = get_distance(x_p1,y_p1,x_p2,y_p2)
print(odległość)
x_p1=0
y_p1=-1
x_p2=0
y_p2=-4
odległość = get_distance(x_p1,y_p1,x_p2,y_p2)
print(odległość)
x_p1=10
y_p1=0
x_p2=5
y_p2=0
odległość = get_distance(x_p1,y_p1,x_p2,y_p2)
print(odległość)
x_p1=0
y_p1=0
x_p2=0
y_p2=0
odległość = get_distance(x_p1,y_p1,x_p2,y_p2)
print(odległość)
x_p1=10000
y_p1=-1000
x_p2=30
y_p2=500
odległość = get_distance(x_p1,y_p1,x_p2,y_p2)
print(odległość)